FROM node:14.15.1-alpine3.12

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY ./app /app

CMD (rm -rf /app/node_modules/ || echo "node_modules doesn't exist, skipping...") && \
    (rm /app/package-lock.json || echo "package-lock doesn't exist, skipping...") && \
    npm install && npm start